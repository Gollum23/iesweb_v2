# -*- coding: utf-8 -*-
__author__ = 'gollum23'
__date__ = '31/05/14'
from django.conf.urls import patterns, url

urlpatterns = patterns(
    'companies.views',
    url(r'^$', 'home_view', name='home'),
    url(r'^add/$', 'add_view', name='add'),
    url(r'^detalle/(?P<company>[-\w]+)/$', 'detail_view', name='detail'),
    url(r'^editar/(?P<company>[-\w]+)/$', 'edit_view', name='edit'),
)
