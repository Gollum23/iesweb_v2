# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.utils.encoding import python_2_unicode_compatible

from helpers.slughifi import slughifi

from core.models import UserTimeStampsModel


@python_2_unicode_compatible
class Company(UserTimeStampsModel):
    name = models.CharField(
        max_length=100,
        verbose_name='Compañia'
    )
    slug = models.SlugField(
        max_length=100,
        verbose_name='Slug',
        blank=True
    )
    nit = models.BigIntegerField(
        verbose_name='Nit'
    )
    digit_verification = models.IntegerField(
        max_length=1,
        verbose_name='',
        blank=True
    )
    address = models.CharField(
        max_length=255,
        verbose_name='Dirección'
    )
    cel_phone = models.BigIntegerField(
        max_length=10,
        verbose_name='Teléfono Celular'
    )
    name_compliance = models.CharField(
        max_length=100,
        verbose_name='Analista Cumplimiento',
        blank=True,
        null=True,
    )
    phone_compliance = models.BigIntegerField(
        max_length=10,
        verbose_name='Teléfono Cumplimiento',
        blank=True,
        null=True
    )
    mail_compliance = models.EmailField(
        max_length=100,
        verbose_name='Email Cumplimiento',
        blank=True,
        null=True
    )
    name_live = models.CharField(
        max_length=100,
        verbose_name='Analista Vida',
        blank=True,
        null=True
    )
    phone_live = models.BigIntegerField(
        max_length=10,
        verbose_name='Teléfono Vida',
        blank=True,
        null=True
    )
    mail_live = models.EmailField(
        max_length=100,
        verbose_name='Email Vida',
        blank=True,
        null=True
    )
    name_car = models.CharField(
        max_length=100,
        verbose_name='Analista Automoviles',
        blank=True,
        null=True
    )
    phone_car = models.BigIntegerField(
        max_length=10,
        verbose_name='Teléfono Automoviles',
        blank=True,
        null=True
    )
    mail_car = models.EmailField(
        max_length=100,
        verbose_name='Email Automoviles',
        blank=True,
        null=True
    )
    name_generals = models.CharField(
        max_length=100,
        verbose_name='Analista Generales',
        blank=True,
        null=True
    )
    phone_generals = models.BigIntegerField(
        max_length=10,
        verbose_name='Teléfono Generales',
        blank=True,
        null=True
    )
    mail_generals = models.EmailField(
        max_length=100,
        verbose_name='Email Generales',
        blank=True,
        null=True
    )

    def __str__(self):
        return '%s' % self.name

    def save(self, *args, **kwargs):
        self.slug = slughifi(self.name)
        super(Company, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Compañia'
        verbose_name_plural = ' Compañias'
