# -*- coding: utf-8 -*-
__author__ = 'gollum23'
__date__ = '31/05/14'
from django import forms

from parsley.decorators import parsleyfy

from .models import Company


@parsleyfy
class CompanyForm(forms.ModelForm):
    name = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Compañia'}
        )
    )
    nit = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'NIT'}
        )
    )
    digit_verification = forms.IntegerField(
        widget=forms.TextInput(
            attrs={
                'placeholder': '',
                'max': 9,
                'min': 0
            }
        )
    )
    address = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Dirección'}
        )
    )
    cel_phone = forms.IntegerField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Teléfono Celular'}
        )
    )
    name_compliance = forms.CharField(
        required=False,
        widget=forms.TextInput(
            attrs={
                'placeholder': 'Analista Cumplimiento',}
        )
    )
    phone_compliance = forms.IntegerField(
        required=False,
        widget=forms.TextInput(
            attrs={'placeholder': 'Teléfono Cumplimiento'}
        )
    )
    mail_compliance = forms.EmailField(
        required=False,
        widget=forms.TextInput(
            attrs={'placeholder': 'Email Cumplimiento'}
        )
    )
    name_live = forms.CharField(
        required=False,
        widget=forms.TextInput(
            attrs={'placeholder': 'Analista Vida'}
        )
    )
    phone_live = forms.IntegerField(
        required=False,
        widget=forms.TextInput(
            attrs={'placeholder': 'Teléfono Vida'}
        )
    )
    mail_live = forms.EmailField(
        required=False,
        widget=forms.TextInput(
            attrs={'placeholder': 'Email Vida'}
        )
    )
    name_car = forms.CharField(
        required=False,
        widget=forms.TextInput(
            attrs={'placeholder': 'Analista Automoviles'}
        )
    )
    phone_car = forms.IntegerField(
        required=False,
        widget=forms.TextInput(
            attrs={'placeholder': 'Teléfono Automoviles'}
        )
    )
    mail_car = forms.EmailField(
        required=False,
        widget=forms.TextInput(
            attrs={'placeholder': 'Email Automoviles'}
        )
    )
    name_generals = forms.CharField(
        required=False,
        widget=forms.TextInput(
            attrs={'placeholder': 'Analista Generales'}
        )
    )
    phone_generals = forms.IntegerField(
        required=False,
        widget=forms.TextInput(
            attrs={'placeholder': 'Teléfono Generales'}
        )
    )
    mail_generals = forms.EmailField(
        required=False,
        widget=forms.TextInput(
            attrs={'placeholder': 'Email Generales'}
        )
    )

    class Meta:
        model = Company