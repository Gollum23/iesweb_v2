# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect

from .forms import CompanyForm
from .models import Company


@login_required(login_url='/')
def home_view(request):
    list_companies = Company.objects.filter(user_id=request.user.id)
    ctx = {
        'list_companies': list_companies
    }
    return render(request, 'companies/home.html', ctx)


@login_required(login_url='/')
def add_view(request):
    form = CompanyForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect(reverse('companies:home'))
    ctx = {
        'form': form
    }
    return render(request, 'companies/add.html', ctx)


@login_required(login_url='/')
def detail_view(request, company):
    company = Company.objects.get(user_id=request.user.id, slug=company)
    ctx = {
        'company': company
    }
    return render(request, 'companies/detail.html', ctx)


@login_required(login_url='/')
def edit_view(request, company):
    company = Company.objects.get(user_id=request.user.id, slug=company)
    form = CompanyForm(request.POST or None, instance=company)
    if form.is_valid():
        form.save()
        return redirect(reverse('companies:home'))
    ctx = {
        'form': form
    }
    return render(request, 'companies/add.html', ctx)