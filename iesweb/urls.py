from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^', include('core.urls', namespace='app')),
    url(r'^companias/', include('companies.urls', namespace='companies')),
    url(r'^productos/', include('products.urls', namespace='products')),
    url(r'^clientes/', include('clients.urls', namespace='clients')),
    url(r'^prospectos/', include('prospects.urls', namespace='prospects')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^ckeditor/', include('ckeditor.urls')),
)
