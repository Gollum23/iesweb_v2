# from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'odof==4y97i3s#8o_w%+*2c6mdogslrag+&$v$zndt%2$f4nty'
#SECRET_KEY = os.environ["SECRETKEY_IESWEB"]

# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'parsley',
    'ckeditor',
    'south',
    'core',
    'companies',
    'products',
    'clients',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'iesweb.urls'

WSGI_APPLICATION = 'iesweb.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'es-CO'

TIME_ZONE = 'America/Bogota'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'

STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "mystatics"),
)
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'templates'),
)


CKEDITOR_MEDIA_PREFIX = 'media/'
CKEDITOR_UPLOAD_PATH = os.path.join(BASE_DIR, 'media')
CKEDITOR_UPLOAD_PREFIX = '/media/'
CKEDITOR_CONFIGS = {
    'default': {
        'toolbar_Basic': [
            ['Bold', 'Italic', 'Underline', 'Strike'],
            ['TextColor', 'BGColor'],
            ['NumberedList', 'BulletedList'],
            ['Smiley', 'SpecialChar'],
        ],
        'toolbar': 'Basic',
        'height': 150,
        'width': '100%',
    },
}

#LOGGING
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

#CACHE
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.PyLibMCCache',
        'LOCATION': '127.0.0.1:11211'
    }
}

DEBUG_TOOLBAR_PATCH_SETTINGS = False

#CELERY
BROKER_URL = 'amqp://gollum23:BAQQME1K@localhost:5672/iesweb'
CELERY_RESULT_BACKEND = 'database'
CELERY_RESULT_DBURI = 'postgresql://gollum23:B@QQME1K@localhost/iesweb_v2'

#Close session to close browser
SESSION_EXPIRE_AT_BROWSER_CLOSE = True
#Close session after 12 hours
SESSION_COOKIE_AGE = 43200

import djcelery
djcelery.setup_loader()