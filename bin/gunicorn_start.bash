#!/bin/sh

NAME="app_iesweb"                                  # Name of the application
#DJANGODIR=/home/gollum23/PycharmProjects/iesweb_v2             # Django project directory
DJANGODIR=/home/gollum23/app.iesweb.co             # Django project directory
#SOCKFILE=/home/gollum23/PycharmProjects/iesweb_v2/run/gunicorn.sock  # we will communicte using this unix socket
SOCKFILE=/home/gollum23/app.iesweb.co/run/gunicorn.sock  # we will communicte using this unix socket
USER=gollum23                                        # the user to run as
GROUP=gollum23                                     # the group to run as
NUM_WORKERS=3                                     # how many worker processes should Gunicorn spawn
DJANGO_SETTINGS_MODULE=iesweb.settings.production             # which settings file should Django use
DJANGO_WSGI_MODULE=iesweb.wsgi                     # WSGI module name

echo "Starting $NAME as `whoami`"

# Activate the virtual environment
cd $DJANGODIR
export WORKON_HOME=$HOME/.virtualenvs
source /usr/local/bin/virtualenvwrapper.sh
workon iesweb
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export SECRETKEY_IESWEB="odof==4y97i3s#8o_w%+*2c6mdogslrag+&$v$zndt%2$f4nty"
export DB_USER="gollum23"
export DB_PASS="eMivsa!NnAcWe7Bq^*t5"
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec /home/gollum23/.virtualenvs/iesweb/bin/gunicorn ${DJANGO_WSGI_MODULE}:application \
  --name $NAME \
  --workers $NUM_WORKERS \
  --user=$USER --group=$GROUP \
  --log-level=debug \
  --bind=unix:$SOCKFILE