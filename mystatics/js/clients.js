/**
 * Created by gollum23 on 27/06/14.
 */
$(document).ready(inicio);
$(document ).foundation();
var product_show;
function inicio(){
    //Oculto el panel
    product_show = false;

    // Manejo del boton para el cambio
    $('#switch_views').on('click', switch_views);
    // Altura de la linea de separación
    var winHeight = $(window).height();
    $('.vertical').css('height', winHeight*0.85);
    // Altura de la linea de separación cuando hay redimensión
    $(window).resize(function(){
        winHeight = $(window).height();
        $('.vertical').css('height', winHeight*0.85);
    });

    // Recuperar los seguimientos de la base de datos
    get_tracing();
    // Recuperar los productos contratados
    get_products_contracted();

//    $('.add-tracing').on('click', add_tracing);
    $('.btn-add-tracing').on('click', send_form_tracing);

//    $('.add-policy').on('click', add_product);
    $('.btn-add-product').on('click', send_form_product)

    // Fix para usar chosen en el modal box de boostrap3
    $('#modal-add-product').on('opened.fndtn.reveal', function(){
        $('.chosen-select', this).chosen();
        $('#id_date_init').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy"
        });
        $('#id_date_finish').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy"
        });

    })
}
function switch_views(e){
    e.preventDefault();
    if (product_show==false){
        $('#tracing-client').css('left', '100%');
        $('#products-client').css('left', '0');
        $('#switch_views').html('<i class="icon-archive"></i> Ver Seguimientos');
        product_show=true;
    }else{
        $('#products-client').css('left', '100%');
        $('#tracing-client').css('left', '0');
        $('#switch_views').html('<i class="icon-briefcase"></i> Ver Pólizas');
        product_show=false;
    }
}

function get_tracing(){
    $.post('/clientes/get-tracing/', {client: CLIENT})
        .success(draw_tracing)
}

function draw_tracing(data){
    var $list_tracing = $('#list_tracing_ajax');
    $list_tracing.html("")
    data.forEach(function(t){
        $list_tracing.append(
            drawing_tracing(t)
        )
    })
}
function drawing_tracing(data){
    var d = new Date(data.date);
    var tracing = '<article class="detail-tracing">'
    tracing += '<p><span>Fecha: </span>'+ moment(d).format("D [de] MMMM [de] YYYY") + '</p>'
    tracing += '<p class="observation-label"><span>Observaciones: </span></p>'
    tracing += '<div class="observations">'+ data.observation+'</div>'
    return tracing;
}

//function add_tracing(e){
//    e.preventDefault();
//    reset_tracing(e);
//    $('#modal-add-tracing').foundation('reveal', 'open');
//}

function send_form_tracing(e){
    e.preventDefault();
    var obs = CKEDITOR.instances.id_observations.getData();
    $('#id_observations').val(obs)
    $.post('/clientes/add-tracing/', $('#tracing_form').serialize())
        .success(
            function(e){
                $('#modal-add-tracing').foundation('reveal', 'close');
                get_tracing();
            }
        )
}

function reset_tracing(e){
    e.preventDefault();
    CKEDITOR.instances.id_observations.setData('');
}

function get_products_contracted(){
    $.post('/clientes/get-products-contracted/', { client: CLIENT })
        .success(draw_products)
}

function draw_products(data){
    var $list_products = $('#list_products_ajax');
    $list_products.html("");
    data.forEach(function(p){
        $list_products.prepend(
            drawing_products(p)
        )
    })
}

function drawing_products(data){
    var di = new Date(data.date_init);
    var df = new Date(data.date_finish)
    var product = '<article class="detail-product">';
    product += '<p class="detail-left"><span>Producto: </span>' + data.product + '</p>';
    product += '<p class="detail-right"><span>Compañia: </span>' + data.company + '</p>';
    product += '<p class="detail-left"><span>Póliza No.: </span>' + data.policy + '</p>';
    product += '<p class="detail-right"><span>Anexo: </span>' + data.policy_annex + '</p>';
    product += '<p class="detail-left"><span>Fecha inicial: </span>' + moment(di).format("D [de] MMMM [de] YYYY") + '</p>';
    product += '<p class="detail-right"><span class="">Fecha final: </span>' + moment(df).format("D [de] MMMM [de] YYYY") + '</p>';
    product += '<p class="detail-left"><span>Vr. Asegurado: </span>' + data.vr_insured + '</p>';
    product += '<p class="detail-right"><span>Vr. Prima</span>' + data.vr_payment + '</p>';
    product += '<p><span>Beneficiario: </span>' + data.beneficiary + '</p>';
    product += '<p class="observation-label"><span>Observaciones: </span></p>';
    product += '<div class="observations">' + data.observation +'</div></article>';
    return product
}

function add_product(e){
    e.preventDefault();
    reset_product_form(e);
    $('#modal-add-product').modal('show')
}

function send_form_product(e){
    e.preventDefault();
    var obs = CKEDITOR.instances.id_observation.getData();
    $('#id_observation').val(obs)
    $.post('/clientes/add-product-contracted/', $('#product-add-form').serialize())
        .success(
            function(e){
                $('#modal-add-product').foundation('reveal', 'close');
                get_products_contracted();
            }
        )
}

function reset_product_form(e){
    e.preventDefault();
    CKEDITOR.instances.id_observation.setData('');
//    $(':input').val('');
}