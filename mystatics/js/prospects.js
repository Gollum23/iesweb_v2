/**
 * Created by gollum23 on 7/9/14.
 */
$(document).ready(inicio);
$(document ).foundation();
var product_show;
function inicio(){

    var winHeight = $(window).height();
    $('.vertical').css('height', winHeight*0.85);
    // Altura de la linea de separación cuando hay redimensión
    $(window).resize(function(){
        winHeight = $(window).height();
        $('.vertical').css('height', winHeight*0.85);
    });

    // Recuperar los seguimientos de la base de datos
    get_tracing();

//    $('.add-tracing').on('click', add_tracing);
    $('.btn-add-tracing').on('click', send_form_tracing);

}

function get_tracing(){
    $.post('/prospectos/get-tracing/', {prospect: PROSPECT})
        .success(draw_tracing)
}

function draw_tracing(data){
    var $list_tracing = $('#list_tracing_ajax');
    $list_tracing.html("")
    data.forEach(function(t){
        $list_tracing.append(
            drawing_tracing(t)
        )
    })
}
function drawing_tracing(data){
    var d = new Date(data.date);
    var tracing = '<article class="detail-tracing">'
    tracing += '<p><span>Fecha: </span>'+ moment(d).format("D [de] MMMM [de] YYYY") + '</p>'
    tracing += '<p class="observation-label"><span>Observaciones: </span></p>'
    tracing += '<div class="observations">'+ data.observation+'</div>'
    return tracing;
}

//function add_tracing(e){
//    e.preventDefault();
//    reset_tracing(e);
//    $('#modal-add-tracing').foundation('reveal', 'open');
//}

function send_form_tracing(e){
    e.preventDefault();
    var obs = CKEDITOR.instances.id_observations.getData();
    $('#id_observations').val(obs)
    $.post('/prospectos/add-tracing/', $('#tracing_form').serialize())
        .success(
            function(e){
                $('#modal-add-tracing').foundation('reveal', 'close');
                get_tracing();
            }
        )
}

function reset_tracing(e){
    e.preventDefault();
    CKEDITOR.instances.id_observations.setData('');
}
