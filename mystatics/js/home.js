/**
 * Created by gollum23 on 5/30/14.
 */
$(document).ready(function() {
    var estado = false;
    $('#info-user').hoverIntent({
        over: desplegarMenu,
        timeout: 300,
        out: overout
    });
    $('body').on('click', ocultarMenu);
    $('a .close').on('click', ocultarMenu);
    function desplegarMenu() {
        estado = true;
        $('#menu-header').slideDown(200)
    }
    function ocultarMenu() {
        if (estado){
            $('#menu-header').slideUp(100);
            estado = false;
        }
    }
    function overout() {

    }
});
