/**
 * Created by gollum23 on 18/06/14.
 */
$(document).ready(inicio);

function inicio(){
    $(".chosen-select").chosen();
    $('table').on('click', 'a', editar_producto);
    $form = $('form');
    $('button').on('click', function(){
        if ($('button').attr('name') == 'edit') {
            save_edit_product();
        }
    });
}

function save_edit_product(){
    $.post($form.attr('action'), $form.serializeArray())
            .done(function(){
                window.location.reload();
    })
}

function editar_producto(data){
    data.preventDefault();
    var prod = $(data.currentTarget).data('id');
    $.get('editar-producto/' + prod, show_product);
}
function show_product(data){
    $('#id_company').val(data.company).trigger('chosen:updated');
    $('#id_department').val(data.department).trigger('chosen:updated');
    $('#id_name').val(data.producto);
    $('button')
            .attr('name', 'edit')
            .html('<i class="icon-disk"></i> Guardar producto');
    $('<input type="hidden" id="id" name="id" value="' + data.id + '">').appendTo($form);

}
