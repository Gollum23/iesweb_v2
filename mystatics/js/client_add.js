/**
 * Created by gollum23 on 7/8/14.
 */

$(document).ready(inicio);

function inicio(){

    $(".chosen-select").chosen({
        disable_search_threshold: 10
    });

    $(".date").datepicker({
        dateFormat: "dd/mm/yy",
        changeYear: true,
        changeMonth: true,
        monthNamesShort:[
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"
        ]
    });
}
