# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url

urlpatterns = patterns(
    'products.views',
    url(r'^$', 'home_view', name='home'),
    url(r'^editar-producto/(?P<prod>\d+)$', 'edit_ajax_view', name='edit_ajax')
)