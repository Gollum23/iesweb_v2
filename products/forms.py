# -*- coding: utf-8 -*-
from django import forms

from .models import Product
from companies.models import Company
from helpers.dictionaries import DEPARTAMENT


class ProductForm(forms.ModelForm):
    company = forms.ModelChoiceField(
        widget=forms.Select(
            attrs={
                'class': 'chosen-select',
                'data-placeholder': 'Seleccione una compañia'
            },

        ),
        label="Compañia",
        queryset=Company.objects.all(),
        empty_label=None
    )
    department = forms.ChoiceField(
        widget=forms.Select(
            attrs={
                'class': 'chosen-select',
                'data-placeholder': 'Seleccione una ramo'
            },
        ),
        label="Ramo",
        choices=DEPARTAMENT
    )
    name = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Producto'}
        ),
        label='Producto'

    )

    class Meta:
        model= Product