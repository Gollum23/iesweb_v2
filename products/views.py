# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.template import RequestContext

from .forms import ProductForm
from .models import Product


@login_required(login_url='/')
def home_view(request):
    form = None
    list_products = Product.objects.filter(user=request.user).order_by('company__name')
    if request.method == 'POST':
        if 'edit' in request.POST:
            product = Product.objects.get(pk=request.POST['id'])
            form = ProductForm(request.POST, instance=product)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(request.path)
        if 'submit' in request.POST:
            form = ProductForm(request.POST)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(request.path)
    else:
        form = ProductForm()
    ctx = {
        'list_products': list_products,
        'form': form
    }
    rctx = RequestContext(request)
    return render(request, 'products/home.html',
                  ctx, context_instance=rctx)


@login_required(login_url='/')
def edit_ajax_view(request, prod):
    if request.is_ajax():
        producto = Product.objects.get(pk=prod)
        return HttpResponse(
            json.dumps({
                'id': producto.id,
                'company': producto.company_id,
                'department': producto.department,
                'producto': producto.name
            }),
            content_type="application/json; charset=uft8"
        )
    else:
        raise Http404