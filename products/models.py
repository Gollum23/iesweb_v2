# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.utils.encoding import python_2_unicode_compatible

from core.models import UserTimeStampsModel

from companies.models import Company
from helpers.dictionaries import DEPARTAMENT


@python_2_unicode_compatible
class Product(UserTimeStampsModel):
    company = models.ForeignKey(
        Company,
        verbose_name='Compañia'
    )
    department = models.CharField(
        max_length=20,
        verbose_name='Ramo',
        choices=DEPARTAMENT
    )
    name = models.CharField(
        max_length=255,
        verbose_name='Producto'
    )

    def __str__(self):
        return '%s - %s' % (self.company.name, self.name)