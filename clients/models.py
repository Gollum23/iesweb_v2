# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models

from core.models import UserTimeStampsModel

from ckeditor.fields import RichTextField
from helpers.slughifi import slughifi
from products.models import Product

from helpers.dictionaries import DOCUMENTS_TYPE


class Client(UserTimeStampsModel):
    name = models.CharField(
        max_length=255,
        verbose_name="Nombre"
    )
    slug = models.SlugField(
        max_length=255,
        verbose_name="Slug",
        blank=True
    )
    type_identification = models.CharField(
        max_length=3,
        verbose_name="Tipo de identificación",
        choices=DOCUMENTS_TYPE
    )
    nit = models.BigIntegerField(
        max_length=10,
        verbose_name="Identificación",
        blank=True
    )
    digit_verification = models.IntegerField(
        max_length=1,
        verbose_name='',
        blank=True
    )
    address = models.CharField(
        max_length=255,
        verbose_name="Dirección"
    )
    city = models.CharField(
        max_length=50,
        verbose_name="Ciudad"
    )
    phone = models.BigIntegerField(
        max_length=10,
        verbose_name="Teléfono",
        blank=True
    )
    cellphone = models.BigIntegerField(
        max_length=10,
        verbose_name="Celular",
        blank=True
    )
    email = models.EmailField(
        max_length=255,
        verbose_name="Correo electrónico",
        blank=True
    )
    contact = models.CharField(
        max_length=255,
        verbose_name="Persona de contacto",
        blank=True
    )
    email_contact = models.EmailField(
        max_length=255,
        verbose_name="Correo electrónico",
        blank=True
    )
    born_date = models.DateField(
        verbose_name="Fecha de nacimiento o creación"
    )
    referred = models.CharField(
        max_length=255,
        verbose_name="Referido por",
        blank=True
    )
    state = models.BooleanField(
        verbose_name="Estado",
        default=True
    )
    is_client = models.BooleanField(
        verbose_name="Cliente",
        default=False
    )
    #TODO pensar en opciones para manejar este campo
    products_to_offer = models.ManyToManyField(
        Product,
        verbose_name="Productos a ofrecer",
        blank=True,
        null=True
    )

    def __unicode__(self):
        return u"%s" % self.name

    def save(self, *args, **kwargs):
        self.slug = slughifi(self.name)
        super(Client, self).save(*args, **kwargs)

    class Meta:
        verbose_name = "Cliente"
        verbose_name_plural = "Clientes"


class Tracing(UserTimeStampsModel):
    client = models.ForeignKey(
        Client,
        verbose_name="Cliente",
        related_name="client_tracing"
    )
    observations = RichTextField(
        verbose_name="Observaciones"
    )

    def __unicode__(self):
        return "%s - %s" % (self.client.name, self.date)

    class Meta:
        verbose_name = "Seguimiento"
        verbose_name_plural = "Seguimientos"
        ordering = ['-date_create']


class ProductContracted(UserTimeStampsModel):
    client = models.ForeignKey(
        Client,
        verbose_name="Cliente",
        related_name="client_product_contracted"
    )
    product = models.ForeignKey(
        Product,
        verbose_name="Producto"
    )
    policy_number = models.CharField(
        max_length=50,
        verbose_name="Número de póliza"
    )
    policy_annex = models.CharField(
        max_length=10,
        verbose_name="Número de anexo",
        blank=True
    )
    date_init = models.DateField(
        verbose_name="Fecha de inicio"
    )
    date_finish = models.DateField(
        verbose_name="Fecha de vencimiento"
    )
    vr_insured = models.BigIntegerField(
        max_length=50,
        verbose_name="Valor asegurado",
        blank=True
    )
    vr_payment = models.BigIntegerField(
        max_length=50,
        verbose_name="Valor prima"
    )
    beneficiary = models.CharField(
        max_length=255,
        verbose_name="Beneficiario",
        blank=True
    )
    observation = RichTextField(
        verbose_name="Observaciones"
    )
    date_expedition = models.DateField(
        verbose_name="Fecha de expedición"
    )

    def __unicode__(self):
        return "%s - %s" % (self.client.name, self.product.name)

    class Meta:
        verbose_name = "Producto Contratado"
        verbose_name_plural = "Productos Contratados"
