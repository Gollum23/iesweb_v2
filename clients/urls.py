# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url

urlpatterns = patterns(
    'clients.views',
    url(r'^$', 'home_client_view', name='home'),
    # url(r'^(?P<search>.*)$', 'home_filtered_client_view', name='home_filtered_clients'),
    url(r'^add/$', 'add_client_view', name='add'),
    url(r'^detalle/(?P<client>[^/]+)/$', 'detail_client_view', name='detail'),
    url(r'^editar/(?P<client>[^/]+)/$', 'edit_client_view', name='edit'),

    #Peticiones Ajax#
    url(r'^get-tracing/$', 'get_tracing', name='get_tracing_ajax'),
    url(r'^add-tracing/$', 'add_tracing', name='add_tracing_ajax'),
    url(r'^get-products-contracted/$', 'get_products_contracted', name='get_policies_ajax'),
    url(r'^add-product-contracted/$', 'add_product_contracted', name='add_product_contracted_ajax'),
)