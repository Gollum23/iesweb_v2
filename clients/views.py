# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
from datetime import timedelta
from django.core.serializers.json import DjangoJSONEncoder
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, Http404
from django.shortcuts import render, redirect
from django.db.models import Q


from .models import Client, Tracing, ProductContracted
from .forms import ClientForm, TracingForm, ProductContractForm


@login_required(login_url='/')
def home_client_view(request):
    list_clients = Client.objects.filter(state=True, is_client=True, user=request.user)
    ctx = {
        'list_clients': list_clients,
    }
    return render(request, 'clients/home.html', ctx)


@login_required(login_url="/")
def home_filtered_client_view(request, search):
    if request.is_ajax():
        q = search
        if q is not None and q is not u'':
            list_clients_ajax = Client.objects.filter(
                Q(name__icontains=q) | Q(email__icontains=q) & Q(state=True) & Q(is_client=True)
            )
        else:
            list_clients_ajax = Client.objects.filter(state=True, is_client=True)
    ctx = {
        'list_clients_ajax': list_clients_ajax
    }
    return render(request, 'clients/home.html', ctx)


@login_required(login_url='/')
def add_client_view(request):
    form = ClientForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect(reverse('clients:home'))
    ctx = {
        'form': form
    }
    return render(request, 'clients/add_edit.html', ctx)


@login_required(login_url='/')
def detail_client_view(request, client):
    client = Client.objects.get(nit=client)
    form_tracing = TracingForm()
    form_product = ProductContractForm()
    ctx = {
        'client': client,
        'formTracing': form_tracing,
        'formProduct': form_product
    }
    return render(request, 'clients/detail.html', ctx)


@login_required(login_url='/')
def edit_client_view(request, client):
    client = Client.objects.get(nit=client)
    form = ClientForm(request.POST or None, instance=client)
    if form.is_valid():
        form.save()
        return redirect(reverse('clients:home'))
    ctx = {
        'form': form
    }
    return render(request, 'clients/add_edit.html', ctx)


###########PETICIONES AJAX##################
@login_required(login_url='/')
def get_tracing(req):
    if req.is_ajax() and req.method == 'POST':
        tracing = Tracing.objects.filter(client__id=req.POST['client'])
        list_tracing = []
        for t in tracing:
            date_t = t.date_create - timedelta(hours=5)
            track = {
                'id': t.id,
                'date': date_t,
                'observation': t.observations
            }
            list_tracing.append(track)
        return HttpResponse(
            json.dumps(list_tracing, cls=DjangoJSONEncoder),
            content_type='application/json; chartset=utf8'
        )
    else:
        raise Http404


@login_required(login_url='/')
def add_tracing(request):
    if request.is_ajax() and request.method == 'POST':
        form = TracingForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponse(status=200)
    else:
        raise Http404


@login_required(login_url='/')
def get_products_contracted(request):
    if request.is_ajax() and request.method == 'POST':
        productscontracted = ProductContracted.objects.filter(client__id=request.POST['client'])
        list_productscontracted = []
        for p in productscontracted:
            date_init = p.date_init - timedelta(hours=5)
            date_finish = p.date_finish - timedelta(hours=5)
            product = {
                'id': p.id,
                'product': p.product.name,
                'company': p.product.company.name,
                'policy': p.policy_number,
                'policy_annex': p.policy_annex,
                'date_init': date_init,
                'date_finish': date_finish,
                'vr_insured': p.vr_insured,
                'vr_payment': p.vr_payment,
                'beneficiary': p.beneficiary,
                'observation': p.observation
            }
            list_productscontracted.append(product)
        return HttpResponse(
            json.dumps(list_productscontracted, cls=DjangoJSONEncoder),
            content_type='application/json; chartset=utf8'
        )
    else:
        raise Http404


@login_required(login_url='/')
def add_product_contracted(request):
    if request.is_ajax() and request.method == 'POST':
        form = ProductContractForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponse(status=200)
    else:
        raise Http404
