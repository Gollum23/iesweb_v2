# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Client'
        db.create_table(u'clients_client', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=255, blank=True)),
            ('type_identification', self.gf('django.db.models.fields.CharField')(max_length=3)),
            ('nit', self.gf('django.db.models.fields.BigIntegerField')(max_length=10, blank=True)),
            ('digit_verification', self.gf('django.db.models.fields.IntegerField')(max_length=1, blank=True)),
            ('address', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('phone', self.gf('django.db.models.fields.BigIntegerField')(max_length=10, blank=True)),
            ('cellphone', self.gf('django.db.models.fields.CharField')(max_length=10, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=255, blank=True)),
            ('contact', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('email_contact', self.gf('django.db.models.fields.EmailField')(max_length=255, blank=True)),
            ('born_date', self.gf('django.db.models.fields.DateField')()),
            ('referred', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('state', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('is_client', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'clients', ['Client'])

        # Adding M2M table for field products_to_offer on 'Client'
        m2m_table_name = db.shorten_name(u'clients_client_products_to_offer')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('client', models.ForeignKey(orm[u'clients.client'], null=False)),
            ('product', models.ForeignKey(orm[u'products.product'], null=False))
        ))
        db.create_unique(m2m_table_name, ['client_id', 'product_id'])

        # Adding model 'Tracing'
        db.create_table(u'clients_tracing', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('client', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'client_tracing', to=orm['clients.Client'])),
            ('date', self.gf('django.db.models.fields.DateField')(auto_now_add=True, blank=True)),
            ('observations', self.gf('ckeditor.fields.RichTextField')()),
        ))
        db.send_create_signal(u'clients', ['Tracing'])

        # Adding model 'ProductContracted'
        db.create_table(u'clients_productcontracted', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('client', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'client_product_contracted', to=orm['clients.Client'])),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['products.Product'])),
            ('policy_number', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('policy_annex', self.gf('django.db.models.fields.CharField')(max_length=10, blank=True)),
            ('date_init', self.gf('django.db.models.fields.DateField')()),
            ('date_finish', self.gf('django.db.models.fields.DateField')()),
            ('vr_insured', self.gf('django.db.models.fields.BigIntegerField')(max_length=50, blank=True)),
            ('vr_payment', self.gf('django.db.models.fields.BigIntegerField')(max_length=50)),
            ('beneficiary', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('observation', self.gf('ckeditor.fields.RichTextField')()),
        ))
        db.send_create_signal(u'clients', ['ProductContracted'])


    def backwards(self, orm):
        # Deleting model 'Client'
        db.delete_table(u'clients_client')

        # Removing M2M table for field products_to_offer on 'Client'
        db.delete_table(db.shorten_name(u'clients_client_products_to_offer'))

        # Deleting model 'Tracing'
        db.delete_table(u'clients_tracing')

        # Deleting model 'ProductContracted'
        db.delete_table(u'clients_productcontracted')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'clients.client': {
            'Meta': {'object_name': 'Client'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'born_date': ('django.db.models.fields.DateField', [], {}),
            'cellphone': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'contact': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'digit_verification': ('django.db.models.fields.IntegerField', [], {'max_length': '1', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '255', 'blank': 'True'}),
            'email_contact': ('django.db.models.fields.EmailField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_client': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'nit': ('django.db.models.fields.BigIntegerField', [], {'max_length': '10', 'blank': 'True'}),
            'phone': ('django.db.models.fields.BigIntegerField', [], {'max_length': '10', 'blank': 'True'}),
            'products_to_offer': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['products.Product']", 'null': 'True', 'blank': 'True'}),
            'referred': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '255', 'blank': 'True'}),
            'state': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'type_identification': ('django.db.models.fields.CharField', [], {'max_length': '3'})
        },
        u'clients.productcontracted': {
            'Meta': {'object_name': 'ProductContracted'},
            'beneficiary': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'client': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'client_product_contracted'", 'to': u"orm['clients.Client']"}),
            'date_finish': ('django.db.models.fields.DateField', [], {}),
            'date_init': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'observation': ('ckeditor.fields.RichTextField', [], {}),
            'policy_annex': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'}),
            'policy_number': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['products.Product']"}),
            'vr_insured': ('django.db.models.fields.BigIntegerField', [], {'max_length': '50', 'blank': 'True'}),
            'vr_payment': ('django.db.models.fields.BigIntegerField', [], {'max_length': '50'})
        },
        u'clients.tracing': {
            'Meta': {'ordering': "[u'-date']", 'object_name': 'Tracing'},
            'client': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'client_tracing'", 'to': u"orm['clients.Client']"}),
            'date': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'observations': ('ckeditor.fields.RichTextField', [], {})
        },
        u'companies.company': {
            'Meta': {'object_name': 'Company'},
            'address': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'cel_phone': ('django.db.models.fields.BigIntegerField', [], {'max_length': '10'}),
            'date_create': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date_update': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'digit_verification': ('django.db.models.fields.IntegerField', [], {'max_length': '1', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mail_car': ('django.db.models.fields.EmailField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'mail_compliance': ('django.db.models.fields.EmailField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'mail_generals': ('django.db.models.fields.EmailField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'mail_live': ('django.db.models.fields.EmailField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name_car': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'name_compliance': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'name_generals': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'name_live': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'nit': ('django.db.models.fields.BigIntegerField', [], {}),
            'phone_car': ('django.db.models.fields.BigIntegerField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'phone_compliance': ('django.db.models.fields.BigIntegerField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'phone_generals': ('django.db.models.fields.BigIntegerField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'phone_live': ('django.db.models.fields.BigIntegerField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '100', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'products.product': {
            'Meta': {'object_name': 'Product'},
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['companies.Company']"}),
            'department': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['clients']