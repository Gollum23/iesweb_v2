# -*- coding: utf-8 -*-
__author__ = 'gollum23'
from django import forms

from parsley.decorators import parsleyfy

from .models import Client, Tracing, ProductContracted
from products.models import Product
from helpers.dictionaries import DOCUMENTS_TYPE, BOOLEAN_CHOICES


@parsleyfy
class ClientForm(forms.ModelForm):
    name = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Nombre completo o razón social'}
        )
    )
    type_identification = forms.ChoiceField(
        widget=forms.Select(
            attrs={
                'class': 'chosen-select',
                'data-placeholder': 'Seleccione...',
                'placeholder': 'Seleccione...'
            }
        ),
        choices=DOCUMENTS_TYPE
    )
    nit = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Identificación'}
        )
    )
    digit_verification = forms.IntegerField(
        widget=forms.TextInput(
            attrs={
                'placeholder': '',
                'max': 9,
                'min': 0
            }
        ),
        required=False
    )
    address = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Dirección'}
        )
    )
    city = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Ciudad'}
        )
    )
    phone = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Teléfono'}
        )
    )
    cellphone = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Teléfono celular'}
        )
    )
    email = forms.EmailField(
        widget=forms.EmailInput(
            attrs={'placeholder': 'Correo electrónico'}
        )
    )
    born_date = forms.DateField(
        widget=forms.DateInput(
            attrs={'class': 'date', 'placeholder': 'Fecha de nacimiento / creación'}
        )
    )
    contact = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Persona de contacto'}
        ),
        required=False
    )
    email_contact = forms.EmailField(
        widget=forms.EmailInput(
            attrs={'placeholder': 'Correo electrónico de contacto'}
        ),
        required=False
    )
    referred = forms.CharField(
        widget=forms.TextInput(
            attrs={'placeholder': 'Referido por'}
        ),
        required=False
    )
    state = forms.CharField(
        widget=forms.Select(
            choices=BOOLEAN_CHOICES
        ),
        label='Estado activo (Si/No)',
        initial=True
    )
    is_client = forms.CharField(
        widget=forms.Select(
            choices=BOOLEAN_CHOICES
        ),
        label='Es cliente (Si/no)',
        initial=True
    )

    class Meta:
        model = Client


@parsleyfy
class TracingForm(forms.ModelForm):
    class Meta:
        model = Tracing


@parsleyfy
class ProductContractForm(forms.ModelForm):
    product = forms.ModelChoiceField(
        widget=forms.Select(
            attrs={
                'class': 'chosen-select',
                'data-placeholder': 'Seleccione un producto'
            },

        ),
        label="Producto",
        queryset=Product.objects.all(),
        empty_label=None
    )

    class Meta:
        model = ProductContracted