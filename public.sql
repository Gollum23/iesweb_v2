/*
Navicat PGSQL Data Transfer

Source Server         : Postgresql_localhost
Source Server Version : 90304
Source Host           : localhost:5432
Source Database       : iesweb_v2
Source Schema         : public

Target Server Type    : PGSQL
Target Server Version : 90304
File Encoding         : 65001

Date: 2014-05-30 22:11:41
*/


-- ----------------------------
-- Sequence structure for "public"."auth_group_id_seq"
-- ----------------------------
DROP SEQUENCE "public"."auth_group_id_seq";
CREATE SEQUENCE "public"."auth_group_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for "public"."auth_group_permissions_id_seq"
-- ----------------------------
DROP SEQUENCE "public"."auth_group_permissions_id_seq";
CREATE SEQUENCE "public"."auth_group_permissions_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for "public"."auth_permission_id_seq"
-- ----------------------------
DROP SEQUENCE "public"."auth_permission_id_seq";
CREATE SEQUENCE "public"."auth_permission_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 30
 CACHE 1;

-- ----------------------------
-- Sequence structure for "public"."auth_user_groups_id_seq"
-- ----------------------------
DROP SEQUENCE "public"."auth_user_groups_id_seq";
CREATE SEQUENCE "public"."auth_user_groups_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for "public"."auth_user_id_seq"
-- ----------------------------
DROP SEQUENCE "public"."auth_user_id_seq";
CREATE SEQUENCE "public"."auth_user_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for "public"."auth_user_user_permissions_id_seq"
-- ----------------------------
DROP SEQUENCE "public"."auth_user_user_permissions_id_seq";
CREATE SEQUENCE "public"."auth_user_user_permissions_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for "public"."core_ieswebclient_id_seq"
-- ----------------------------
DROP SEQUENCE "public"."core_ieswebclient_id_seq";
CREATE SEQUENCE "public"."core_ieswebclient_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for "public"."core_ieswebclient_modules_id_seq"
-- ----------------------------
DROP SEQUENCE "public"."core_ieswebclient_modules_id_seq";
CREATE SEQUENCE "public"."core_ieswebclient_modules_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for "public"."core_module_id_seq"
-- ----------------------------
DROP SEQUENCE "public"."core_module_id_seq";
CREATE SEQUENCE "public"."core_module_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for "public"."core_paymenthistory_id_seq"
-- ----------------------------
DROP SEQUENCE "public"."core_paymenthistory_id_seq";
CREATE SEQUENCE "public"."core_paymenthistory_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for "public"."django_admin_log_id_seq"
-- ----------------------------
DROP SEQUENCE "public"."django_admin_log_id_seq";
CREATE SEQUENCE "public"."django_admin_log_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 1
 CACHE 1;

-- ----------------------------
-- Sequence structure for "public"."django_content_type_id_seq"
-- ----------------------------
DROP SEQUENCE "public"."django_content_type_id_seq";
CREATE SEQUENCE "public"."django_content_type_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 10
 CACHE 1;

-- ----------------------------
-- Sequence structure for "public"."south_migrationhistory_id_seq"
-- ----------------------------
DROP SEQUENCE "public"."south_migrationhistory_id_seq";
CREATE SEQUENCE "public"."south_migrationhistory_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 3
 CACHE 1;

-- ----------------------------
-- Table structure for "public"."auth_group"
-- ----------------------------
DROP TABLE "public"."auth_group";
CREATE TABLE "public"."auth_group" (
"id" int4 DEFAULT nextval('auth_group_id_seq'::regclass) NOT NULL,
"name" varchar(80) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of auth_group
-- ----------------------------

-- ----------------------------
-- Table structure for "public"."auth_group_permissions"
-- ----------------------------
DROP TABLE "public"."auth_group_permissions";
CREATE TABLE "public"."auth_group_permissions" (
"id" int4 DEFAULT nextval('auth_group_permissions_id_seq'::regclass) NOT NULL,
"group_id" int4 NOT NULL,
"permission_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of auth_group_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for "public"."auth_permission"
-- ----------------------------
DROP TABLE "public"."auth_permission";
CREATE TABLE "public"."auth_permission" (
"id" int4 DEFAULT nextval('auth_permission_id_seq'::regclass) NOT NULL,
"name" varchar(50) NOT NULL,
"content_type_id" int4 NOT NULL,
"codename" varchar(100) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of auth_permission
-- ----------------------------
INSERT INTO "public"."auth_permission" VALUES ('1', 'Can add log entry', '1', 'add_logentry');
INSERT INTO "public"."auth_permission" VALUES ('2', 'Can change log entry', '1', 'change_logentry');
INSERT INTO "public"."auth_permission" VALUES ('3', 'Can delete log entry', '1', 'delete_logentry');
INSERT INTO "public"."auth_permission" VALUES ('4', 'Can add permission', '2', 'add_permission');
INSERT INTO "public"."auth_permission" VALUES ('5', 'Can change permission', '2', 'change_permission');
INSERT INTO "public"."auth_permission" VALUES ('6', 'Can delete permission', '2', 'delete_permission');
INSERT INTO "public"."auth_permission" VALUES ('7', 'Can add group', '3', 'add_group');
INSERT INTO "public"."auth_permission" VALUES ('8', 'Can change group', '3', 'change_group');
INSERT INTO "public"."auth_permission" VALUES ('9', 'Can delete group', '3', 'delete_group');
INSERT INTO "public"."auth_permission" VALUES ('10', 'Can add user', '4', 'add_user');
INSERT INTO "public"."auth_permission" VALUES ('11', 'Can change user', '4', 'change_user');
INSERT INTO "public"."auth_permission" VALUES ('12', 'Can delete user', '4', 'delete_user');
INSERT INTO "public"."auth_permission" VALUES ('13', 'Can add content type', '5', 'add_contenttype');
INSERT INTO "public"."auth_permission" VALUES ('14', 'Can change content type', '5', 'change_contenttype');
INSERT INTO "public"."auth_permission" VALUES ('15', 'Can delete content type', '5', 'delete_contenttype');
INSERT INTO "public"."auth_permission" VALUES ('16', 'Can add session', '6', 'add_session');
INSERT INTO "public"."auth_permission" VALUES ('17', 'Can change session', '6', 'change_session');
INSERT INTO "public"."auth_permission" VALUES ('18', 'Can delete session', '6', 'delete_session');
INSERT INTO "public"."auth_permission" VALUES ('19', 'Can add migration history', '7', 'add_migrationhistory');
INSERT INTO "public"."auth_permission" VALUES ('20', 'Can change migration history', '7', 'change_migrationhistory');
INSERT INTO "public"."auth_permission" VALUES ('21', 'Can delete migration history', '7', 'delete_migrationhistory');
INSERT INTO "public"."auth_permission" VALUES ('22', 'Can add Módulo', '8', 'add_module');
INSERT INTO "public"."auth_permission" VALUES ('23', 'Can change Módulo', '8', 'change_module');
INSERT INTO "public"."auth_permission" VALUES ('24', 'Can delete Módulo', '8', 'delete_module');
INSERT INTO "public"."auth_permission" VALUES ('25', 'Can add Usuario', '9', 'add_ieswebclient');
INSERT INTO "public"."auth_permission" VALUES ('26', 'Can change Usuario', '9', 'change_ieswebclient');
INSERT INTO "public"."auth_permission" VALUES ('27', 'Can delete Usuario', '9', 'delete_ieswebclient');
INSERT INTO "public"."auth_permission" VALUES ('28', 'Can add Historial de pago', '10', 'add_paymenthistory');
INSERT INTO "public"."auth_permission" VALUES ('29', 'Can change Historial de pago', '10', 'change_paymenthistory');
INSERT INTO "public"."auth_permission" VALUES ('30', 'Can delete Historial de pago', '10', 'delete_paymenthistory');

-- ----------------------------
-- Table structure for "public"."auth_user"
-- ----------------------------
DROP TABLE "public"."auth_user";
CREATE TABLE "public"."auth_user" (
"id" int4 DEFAULT nextval('auth_user_id_seq'::regclass) NOT NULL,
"password" varchar(128) NOT NULL,
"last_login" timestamptz(6) NOT NULL,
"is_superuser" bool NOT NULL,
"username" varchar(30) NOT NULL,
"first_name" varchar(30) NOT NULL,
"last_name" varchar(30) NOT NULL,
"email" varchar(75) NOT NULL,
"is_staff" bool NOT NULL,
"is_active" bool NOT NULL,
"date_joined" timestamptz(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of auth_user
-- ----------------------------
INSERT INTO "public"."auth_user" VALUES ('1', 'pbkdf2_sha256$12000$y5IFZAjXc6Yl$7DI4f2BAlg+Jul1ZvO2tTJnRAbjdHkCkiXjSlSqJOWI=', '2014-05-30 22:11:13.343965-05', 't', 'gollum23', '', '', 'gollum23@gmail.com', 't', 't', '2014-05-30 22:11:13.343965-05');

-- ----------------------------
-- Table structure for "public"."auth_user_groups"
-- ----------------------------
DROP TABLE "public"."auth_user_groups";
CREATE TABLE "public"."auth_user_groups" (
"id" int4 DEFAULT nextval('auth_user_groups_id_seq'::regclass) NOT NULL,
"user_id" int4 NOT NULL,
"group_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of auth_user_groups
-- ----------------------------

-- ----------------------------
-- Table structure for "public"."auth_user_user_permissions"
-- ----------------------------
DROP TABLE "public"."auth_user_user_permissions";
CREATE TABLE "public"."auth_user_user_permissions" (
"id" int4 DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass) NOT NULL,
"user_id" int4 NOT NULL,
"permission_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of auth_user_user_permissions
-- ----------------------------

-- ----------------------------
-- Table structure for "public"."core_ieswebclient"
-- ----------------------------
DROP TABLE "public"."core_ieswebclient";
CREATE TABLE "public"."core_ieswebclient" (
"id" int4 DEFAULT nextval('core_ieswebclient_id_seq'::regclass) NOT NULL,
"date_create" timestamptz(6) NOT NULL,
"date_update" timestamptz(6) NOT NULL,
"user_id" int4 NOT NULL,
"client_id" int4 NOT NULL,
"paid_until" date NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of core_ieswebclient
-- ----------------------------

-- ----------------------------
-- Table structure for "public"."core_ieswebclient_modules"
-- ----------------------------
DROP TABLE "public"."core_ieswebclient_modules";
CREATE TABLE "public"."core_ieswebclient_modules" (
"id" int4 DEFAULT nextval('core_ieswebclient_modules_id_seq'::regclass) NOT NULL,
"ieswebclient_id" int4 NOT NULL,
"module_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of core_ieswebclient_modules
-- ----------------------------

-- ----------------------------
-- Table structure for "public"."core_module"
-- ----------------------------
DROP TABLE "public"."core_module";
CREATE TABLE "public"."core_module" (
"id" int4 DEFAULT nextval('core_module_id_seq'::regclass) NOT NULL,
"date_create" timestamptz(6) NOT NULL,
"date_update" timestamptz(6) NOT NULL,
"name" varchar(50) NOT NULL,
"slug" varchar(50) NOT NULL,
"url" varchar(50) NOT NULL,
"icon" varchar(20) NOT NULL,
"description" text NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of core_module
-- ----------------------------

-- ----------------------------
-- Table structure for "public"."core_paymenthistory"
-- ----------------------------
DROP TABLE "public"."core_paymenthistory";
CREATE TABLE "public"."core_paymenthistory" (
"id" int4 DEFAULT nextval('core_paymenthistory_id_seq'::regclass) NOT NULL,
"date_create" timestamptz(6) NOT NULL,
"date_update" timestamptz(6) NOT NULL,
"client_id" int4 NOT NULL,
"payment_date" date NOT NULL,
"payment_method" varchar(50) NOT NULL,
"payment_amount" int8 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of core_paymenthistory
-- ----------------------------

-- ----------------------------
-- Table structure for "public"."django_admin_log"
-- ----------------------------
DROP TABLE "public"."django_admin_log";
CREATE TABLE "public"."django_admin_log" (
"id" int4 DEFAULT nextval('django_admin_log_id_seq'::regclass) NOT NULL,
"action_time" timestamptz(6) NOT NULL,
"user_id" int4 NOT NULL,
"content_type_id" int4,
"object_id" text,
"object_repr" varchar(200) NOT NULL,
"action_flag" int2 NOT NULL,
"change_message" text NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of django_admin_log
-- ----------------------------

-- ----------------------------
-- Table structure for "public"."django_content_type"
-- ----------------------------
DROP TABLE "public"."django_content_type";
CREATE TABLE "public"."django_content_type" (
"id" int4 DEFAULT nextval('django_content_type_id_seq'::regclass) NOT NULL,
"name" varchar(100) NOT NULL,
"app_label" varchar(100) NOT NULL,
"model" varchar(100) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of django_content_type
-- ----------------------------
INSERT INTO "public"."django_content_type" VALUES ('1', 'log entry', 'admin', 'logentry');
INSERT INTO "public"."django_content_type" VALUES ('2', 'permission', 'auth', 'permission');
INSERT INTO "public"."django_content_type" VALUES ('3', 'group', 'auth', 'group');
INSERT INTO "public"."django_content_type" VALUES ('4', 'user', 'auth', 'user');
INSERT INTO "public"."django_content_type" VALUES ('5', 'content type', 'contenttypes', 'contenttype');
INSERT INTO "public"."django_content_type" VALUES ('6', 'session', 'sessions', 'session');
INSERT INTO "public"."django_content_type" VALUES ('7', 'migration history', 'south', 'migrationhistory');
INSERT INTO "public"."django_content_type" VALUES ('8', 'Módulo', 'core', 'module');
INSERT INTO "public"."django_content_type" VALUES ('9', 'Usuario', 'core', 'ieswebclient');
INSERT INTO "public"."django_content_type" VALUES ('10', 'Historial de pago', 'core', 'paymenthistory');

-- ----------------------------
-- Table structure for "public"."django_session"
-- ----------------------------
DROP TABLE "public"."django_session";
CREATE TABLE "public"."django_session" (
"session_key" varchar(40) NOT NULL,
"session_data" text NOT NULL,
"expire_date" timestamptz(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of django_session
-- ----------------------------

-- ----------------------------
-- Table structure for "public"."south_migrationhistory"
-- ----------------------------
DROP TABLE "public"."south_migrationhistory";
CREATE TABLE "public"."south_migrationhistory" (
"id" int4 DEFAULT nextval('south_migrationhistory_id_seq'::regclass) NOT NULL,
"app_name" varchar(255) NOT NULL,
"migration" varchar(255) NOT NULL,
"applied" timestamptz(6) NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of south_migrationhistory
-- ----------------------------
INSERT INTO "public"."south_migrationhistory" VALUES ('1', 'core', '0001_initial', '2014-05-30 22:11:26.352593-05');
INSERT INTO "public"."south_migrationhistory" VALUES ('2', 'django_extensions', '0001_empty', '2014-05-30 22:11:26.453697-05');
INSERT INTO "public"."south_migrationhistory" VALUES ('3', 'django_extensions', '0002_initial', '2014-05-30 22:11:26.462715-05');

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------
ALTER SEQUENCE "public"."auth_group_id_seq" OWNED BY "auth_group"."id";
ALTER SEQUENCE "public"."auth_group_permissions_id_seq" OWNED BY "auth_group_permissions"."id";
ALTER SEQUENCE "public"."auth_permission_id_seq" OWNED BY "auth_permission"."id";
ALTER SEQUENCE "public"."auth_user_groups_id_seq" OWNED BY "auth_user_groups"."id";
ALTER SEQUENCE "public"."auth_user_id_seq" OWNED BY "auth_user"."id";
ALTER SEQUENCE "public"."auth_user_user_permissions_id_seq" OWNED BY "auth_user_user_permissions"."id";
ALTER SEQUENCE "public"."core_ieswebclient_id_seq" OWNED BY "core_ieswebclient"."id";
ALTER SEQUENCE "public"."core_ieswebclient_modules_id_seq" OWNED BY "core_ieswebclient_modules"."id";
ALTER SEQUENCE "public"."core_module_id_seq" OWNED BY "core_module"."id";
ALTER SEQUENCE "public"."core_paymenthistory_id_seq" OWNED BY "core_paymenthistory"."id";
ALTER SEQUENCE "public"."django_admin_log_id_seq" OWNED BY "django_admin_log"."id";
ALTER SEQUENCE "public"."django_content_type_id_seq" OWNED BY "django_content_type"."id";
ALTER SEQUENCE "public"."south_migrationhistory_id_seq" OWNED BY "south_migrationhistory"."id";

-- ----------------------------
-- Indexes structure for table auth_group
-- ----------------------------
CREATE INDEX "auth_group_name_like" ON "public"."auth_group" USING btree ("name" "pg_catalog"."varchar_pattern_ops");

-- ----------------------------
-- Uniques structure for table "public"."auth_group"
-- ----------------------------
ALTER TABLE "public"."auth_group" ADD UNIQUE ("name");

-- ----------------------------
-- Primary Key structure for table "public"."auth_group"
-- ----------------------------
ALTER TABLE "public"."auth_group" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table auth_group_permissions
-- ----------------------------
CREATE INDEX "auth_group_permissions_group_id" ON "public"."auth_group_permissions" USING btree ("group_id");
CREATE INDEX "auth_group_permissions_permission_id" ON "public"."auth_group_permissions" USING btree ("permission_id");

-- ----------------------------
-- Uniques structure for table "public"."auth_group_permissions"
-- ----------------------------
ALTER TABLE "public"."auth_group_permissions" ADD UNIQUE ("group_id", "permission_id");

-- ----------------------------
-- Primary Key structure for table "public"."auth_group_permissions"
-- ----------------------------
ALTER TABLE "public"."auth_group_permissions" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table auth_permission
-- ----------------------------
CREATE INDEX "auth_permission_content_type_id" ON "public"."auth_permission" USING btree ("content_type_id");

-- ----------------------------
-- Uniques structure for table "public"."auth_permission"
-- ----------------------------
ALTER TABLE "public"."auth_permission" ADD UNIQUE ("content_type_id", "codename");

-- ----------------------------
-- Primary Key structure for table "public"."auth_permission"
-- ----------------------------
ALTER TABLE "public"."auth_permission" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table auth_user
-- ----------------------------
CREATE INDEX "auth_user_username_like" ON "public"."auth_user" USING btree ("username" "pg_catalog"."varchar_pattern_ops");

-- ----------------------------
-- Uniques structure for table "public"."auth_user"
-- ----------------------------
ALTER TABLE "public"."auth_user" ADD UNIQUE ("username");

-- ----------------------------
-- Primary Key structure for table "public"."auth_user"
-- ----------------------------
ALTER TABLE "public"."auth_user" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table auth_user_groups
-- ----------------------------
CREATE INDEX "auth_user_groups_group_id" ON "public"."auth_user_groups" USING btree ("group_id");
CREATE INDEX "auth_user_groups_user_id" ON "public"."auth_user_groups" USING btree ("user_id");

-- ----------------------------
-- Uniques structure for table "public"."auth_user_groups"
-- ----------------------------
ALTER TABLE "public"."auth_user_groups" ADD UNIQUE ("user_id", "group_id");

-- ----------------------------
-- Primary Key structure for table "public"."auth_user_groups"
-- ----------------------------
ALTER TABLE "public"."auth_user_groups" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table auth_user_user_permissions
-- ----------------------------
CREATE INDEX "auth_user_user_permissions_permission_id" ON "public"."auth_user_user_permissions" USING btree ("permission_id");
CREATE INDEX "auth_user_user_permissions_user_id" ON "public"."auth_user_user_permissions" USING btree ("user_id");

-- ----------------------------
-- Uniques structure for table "public"."auth_user_user_permissions"
-- ----------------------------
ALTER TABLE "public"."auth_user_user_permissions" ADD UNIQUE ("user_id", "permission_id");

-- ----------------------------
-- Primary Key structure for table "public"."auth_user_user_permissions"
-- ----------------------------
ALTER TABLE "public"."auth_user_user_permissions" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table core_ieswebclient
-- ----------------------------
CREATE INDEX "core_ieswebclient_user_id" ON "public"."core_ieswebclient" USING btree ("user_id");

-- ----------------------------
-- Uniques structure for table "public"."core_ieswebclient"
-- ----------------------------
ALTER TABLE "public"."core_ieswebclient" ADD UNIQUE ("client_id");

-- ----------------------------
-- Primary Key structure for table "public"."core_ieswebclient"
-- ----------------------------
ALTER TABLE "public"."core_ieswebclient" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table core_ieswebclient_modules
-- ----------------------------
CREATE INDEX "core_ieswebclient_modules_ieswebclient_id" ON "public"."core_ieswebclient_modules" USING btree ("ieswebclient_id");
CREATE INDEX "core_ieswebclient_modules_module_id" ON "public"."core_ieswebclient_modules" USING btree ("module_id");

-- ----------------------------
-- Uniques structure for table "public"."core_ieswebclient_modules"
-- ----------------------------
ALTER TABLE "public"."core_ieswebclient_modules" ADD UNIQUE ("ieswebclient_id", "module_id");

-- ----------------------------
-- Primary Key structure for table "public"."core_ieswebclient_modules"
-- ----------------------------
ALTER TABLE "public"."core_ieswebclient_modules" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table core_module
-- ----------------------------
CREATE INDEX "core_module_slug" ON "public"."core_module" USING btree ("slug");
CREATE INDEX "core_module_slug_like" ON "public"."core_module" USING btree ("slug" "pg_catalog"."varchar_pattern_ops");

-- ----------------------------
-- Primary Key structure for table "public"."core_module"
-- ----------------------------
ALTER TABLE "public"."core_module" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table core_paymenthistory
-- ----------------------------
CREATE INDEX "core_paymenthistory_client_id" ON "public"."core_paymenthistory" USING btree ("client_id");

-- ----------------------------
-- Primary Key structure for table "public"."core_paymenthistory"
-- ----------------------------
ALTER TABLE "public"."core_paymenthistory" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table django_admin_log
-- ----------------------------
CREATE INDEX "django_admin_log_content_type_id" ON "public"."django_admin_log" USING btree ("content_type_id");
CREATE INDEX "django_admin_log_user_id" ON "public"."django_admin_log" USING btree ("user_id");

-- ----------------------------
-- Checks structure for table "public"."django_admin_log"
-- ----------------------------
ALTER TABLE "public"."django_admin_log" ADD CHECK (action_flag >= 0);

-- ----------------------------
-- Primary Key structure for table "public"."django_admin_log"
-- ----------------------------
ALTER TABLE "public"."django_admin_log" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table "public"."django_content_type"
-- ----------------------------
ALTER TABLE "public"."django_content_type" ADD UNIQUE ("app_label", "model");

-- ----------------------------
-- Primary Key structure for table "public"."django_content_type"
-- ----------------------------
ALTER TABLE "public"."django_content_type" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table django_session
-- ----------------------------
CREATE INDEX "django_session_expire_date" ON "public"."django_session" USING btree ("expire_date");
CREATE INDEX "django_session_session_key_like" ON "public"."django_session" USING btree ("session_key" "pg_catalog"."varchar_pattern_ops");

-- ----------------------------
-- Primary Key structure for table "public"."django_session"
-- ----------------------------
ALTER TABLE "public"."django_session" ADD PRIMARY KEY ("session_key");

-- ----------------------------
-- Primary Key structure for table "public"."south_migrationhistory"
-- ----------------------------
ALTER TABLE "public"."south_migrationhistory" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Key structure for table "public"."auth_group_permissions"
-- ----------------------------
ALTER TABLE "public"."auth_group_permissions" ADD FOREIGN KEY ("permission_id") REFERENCES "public"."auth_permission" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "public"."auth_group_permissions" ADD FOREIGN KEY ("group_id") REFERENCES "public"."auth_group" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;

-- ----------------------------
-- Foreign Key structure for table "public"."auth_permission"
-- ----------------------------
ALTER TABLE "public"."auth_permission" ADD FOREIGN KEY ("content_type_id") REFERENCES "public"."django_content_type" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;

-- ----------------------------
-- Foreign Key structure for table "public"."auth_user_groups"
-- ----------------------------
ALTER TABLE "public"."auth_user_groups" ADD FOREIGN KEY ("user_id") REFERENCES "public"."auth_user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "public"."auth_user_groups" ADD FOREIGN KEY ("group_id") REFERENCES "public"."auth_group" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;

-- ----------------------------
-- Foreign Key structure for table "public"."auth_user_user_permissions"
-- ----------------------------
ALTER TABLE "public"."auth_user_user_permissions" ADD FOREIGN KEY ("permission_id") REFERENCES "public"."auth_permission" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "public"."auth_user_user_permissions" ADD FOREIGN KEY ("user_id") REFERENCES "public"."auth_user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;

-- ----------------------------
-- Foreign Key structure for table "public"."core_ieswebclient"
-- ----------------------------
ALTER TABLE "public"."core_ieswebclient" ADD FOREIGN KEY ("user_id") REFERENCES "public"."auth_user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "public"."core_ieswebclient" ADD FOREIGN KEY ("client_id") REFERENCES "public"."auth_user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;

-- ----------------------------
-- Foreign Key structure for table "public"."core_ieswebclient_modules"
-- ----------------------------
ALTER TABLE "public"."core_ieswebclient_modules" ADD FOREIGN KEY ("module_id") REFERENCES "public"."core_module" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "public"."core_ieswebclient_modules" ADD FOREIGN KEY ("ieswebclient_id") REFERENCES "public"."core_ieswebclient" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;

-- ----------------------------
-- Foreign Key structure for table "public"."core_paymenthistory"
-- ----------------------------
ALTER TABLE "public"."core_paymenthistory" ADD FOREIGN KEY ("client_id") REFERENCES "public"."core_ieswebclient" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;

-- ----------------------------
-- Foreign Key structure for table "public"."django_admin_log"
-- ----------------------------
ALTER TABLE "public"."django_admin_log" ADD FOREIGN KEY ("user_id") REFERENCES "public"."auth_user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "public"."django_admin_log" ADD FOREIGN KEY ("content_type_id") REFERENCES "public"."django_content_type" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION DEFERRABLE INITIALLY DEFERRED;
