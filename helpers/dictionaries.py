# -*- coding: utf-8 -*-


DEPARTAMENT = (
    ('aut', 'Automoviles'),
    ('cum', 'Cumplimiento'),
    ('gen', 'Generales'),
    ('vid', 'Vida')
)


DOCUMENTS_TYPE = (
    ('nit', 'Nit'),
    ('cc', 'C.C'),
    ('pas', 'Pasaporte'),
    ('ce', 'Cedula de extranjería'),
)


BOOLEAN_CHOICES = (
    ('False', 'No'),
    ('True', 'Si')
)


PAY_METHOD = (
    ('ch', 'Cheque',),
    ('co', 'Consignación',),
    ('ef', 'Efectivo',),
    ('tc', 'Tarjeta Credito',),
)

