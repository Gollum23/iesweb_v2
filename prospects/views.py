from __future__ import unicode_literals
import json
from datetime import timedelta
from django.core.serializers.json import DjangoJSONEncoder
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, Http404
from django.shortcuts import render, redirect
from django.db.models import Q

from clients.models import Client, Tracing, ProductContracted
from .forms import ProspectForm, TracingForm


@login_required(login_url='/')
def home_prospect_view(request):
    list_prospects = Client.objects.filter(state=True, is_client=False, user=request.user)
    ctx = {
        'list_prospects': list_prospects,
    }
    return render(request, 'prospects/home.html', ctx)


@login_required(login_url='/')
def add_prospect_view(request):
    form = ProspectForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect(reverse('prospects:home'))
    ctx = {
        'form': form
    }
    return render(request, 'prospects/add_edit.html', ctx)


@login_required(login_url='/')
def detail_prospect_view(request, client):
    prospect = Client.objects.get(nit=client)
    form_tracing = TracingForm()
    # form_product = ProductContractForm()
    ctx = {
        'prospect': prospect,
        'formTracing': form_tracing
        # 'formProduct': form_product
    }
    return render(request, 'prospects/detail.html', ctx)


@login_required(login_url='/')
def edit_prospect_view(request, client):
    prospect = Client.objects.get(nit=client)
    form = ProspectForm(request.POST or None, instance=prospect)
    if form.is_valid():
        form.save()
        return redirect(reverse('prospects:home'))
    ctx = {
        'form': form
    }
    return render(request, 'prospects/add_edit.html', ctx)


###########PETICIONES AJAX##################
@login_required(login_url='/')
def get_tracing(req):
    if req.is_ajax() and req.method == 'POST':
        tracing = Tracing.objects.filter(client__id=req.POST['prospect'])
        list_tracing = []
        for t in tracing:
            date_t = t.date_create - timedelta(hours=5)
            track = {
                'id': t.id,
                'date': date_t,
                'observation': t.observations
            }
            list_tracing.append(track)
        return HttpResponse(
            json.dumps(list_tracing, cls=DjangoJSONEncoder),
            content_type='application/json; chartset=utf8'
        )
    else:
        raise Http404


@login_required(login_url='/')
def add_tracing(request):
    if request.is_ajax() and request.method == 'POST':
        form = TracingForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponse(status=200)
    else:
        raise Http404