# -*- coding: utf-8 -*-
__author__ = 'gollum23'
__date__ = '7/9/14'
from django.conf.urls import patterns, url

urlpatterns = patterns(
    'prospects.views',
    url(r'^$', 'home_prospect_view', name='home'),
    # url(r'^(?P<search>.*)$', 'home_filtered_client_view', name='home_filtered_clients'),
    url(r'^add/$', 'add_prospect_view', name='add'),
    url(r'^detalle/(?P<client>[^/]+)/$', 'detail_prospect_view', name='detail'),
    url(r'^editar/(?P<client>[^/]+)/$', 'edit_prospect_view', name='edit'),

    #Peticiones Ajax#
    url(r'^get-tracing/$', 'get_tracing', name='get_tracing_ajax'),
    url(r'^add-tracing/$', 'add_tracing', name='add_tracing_ajax'),
)