# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models
from django.utils.encoding import python_2_unicode_compatible

from ckeditor.fields import RichTextField
from helpers.slughifi import slughifi
from helpers.dictionaries import PAY_METHOD


class UserTimeStampsModel(models.Model):
    """
    Clase abstracta para el manejo de usuario y  fechas de creación y actualización de registros
    """
    date_create = models.DateTimeField(
        auto_now_add=True
    )
    date_update = models.DateTimeField(
        auto_now=True
    )
    user = models.ForeignKey(
        User,
        related_name="%(app_label)s_%(class)s_ownership"
    )

    class Meta:
        abstract = True


class TimeStampsModel(models.Model):
    """
    Clase abstracta para el manejo de fechas de creación y actualización de registros
    """
    date_create = models.DateTimeField(
        auto_now_add=True
    )
    date_update = models.DateTimeField(
        auto_now=True
    )

    class Meta:
        abstract = True


@python_2_unicode_compatible
class Module(TimeStampsModel):
    """
    Definición de modulos, descripción, url e icono
    """
    name = models.CharField(
        max_length=50,
        verbose_name='Nombre Modulo'
    )
    slug = models.SlugField(
        max_length=50,
        verbose_name=' Slug',
        blank=True
    )
    url = models.CharField(
        max_length=50,
        verbose_name='URL'
    )
    icon = models.CharField(
        max_length=20,
        verbose_name='Icono'
    )
    description = RichTextField(
        verbose_name='Descripción'
    )

    def __str__(self):
        return '%s' % self.name

    def save(self, *args, **kwargs):
        self.slug = slughifi(self.name)
        super(Module, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Módulo'
        verbose_name_plural = 'Módulos'


@python_2_unicode_compatible
class IeswebClient(UserTimeStampsModel):
    """
    Administración de clientes, vigencia de suscripción
    """
    client = models.OneToOneField(
        User,
        related_name='client',
        verbose_name='Cliente'
    )
    paid_until = models.DateField(
        verbose_name='Pagado hasta'
    )
    modules = models.ManyToManyField(
        Module,
        verbose_name='Modulos'
    )
    phone = models.BigIntegerField(
        verbose_name='Teléfono'
    )
    cellphone = models.BigIntegerField(
        verbose_name='Celular'
    )
    mail = models.EmailField(
        verbose_name='Correo electrónico'
    )
    address = models.CharField(
        verbose_name='Dirección',
        max_length='254'
    )

    def __str__(self):
        return '%s' % self.client.get_full_name()

    class Meta:
        verbose_name = 'Usuario'
        verbose_name_plural = 'Usuarios'


@python_2_unicode_compatible
class PaymentHistory(TimeStampsModel):
    """
    Historial de pagos realizados por los clientes
    """
    client = models.ForeignKey(
        IeswebClient,
        verbose_name='Cliente'
    )
    payment_date = models.DateField(
        verbose_name='Fecha de Pago'
    )
    payment_method = models.CharField(
        max_length=50,
        verbose_name='Medio de Pago',
        choices=PAY_METHOD
    )
    payment_amount = models.BigIntegerField(
        verbose_name='Valor pagado'
    )

    def __str__(self):
        return '%s' % self.client.user.get_full_name()

    class Meta:
        verbose_name = 'Historial de pago'
        verbose_name_plural = 'Historial de pagos'