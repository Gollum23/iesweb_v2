# -*- coding: utf-8 -*-
from datetime import date, timedelta
# from django.contrib import messages
from django.contrib.auth import login, logout
from django.db.models import Count
from django.shortcuts import redirect
from django.shortcuts import render
from django.utils import timezone

from .forms import MyAuthenticationForm
from .models import IeswebClient

from clients.models import Tracing, ProductContracted
from companies.models import Company
from products.models import Product


def home_view(request):
    if request.user.is_authenticated():
        ies_client = IeswebClient.objects.get(client=request.user)
        now = timezone.now()  # Get current day with timezone
        after = now + timedelta(days=30)  # set limit to search + 30 days
        before = now - timedelta(days=30)  # set limit to search - 30 days
        # Validate if suscription no expires
        if ies_client.paid_until > date.today():
            list_tracing = Tracing.objects.filter(user=request.user).order_by('-date_create')[:5]
            list_module = ies_client.modules
            list_expiries = ProductContracted.objects.filter(
                date_finish__range=(now, after)
            )
            count_for_companies_latest = ProductContracted.objects.values('product__company__name')\
                .annotate(numero=Count('product'))\
                .filter(date_expedition__range=(before, now))
            count_for_products_latest = ProductContracted.objects.values('product__name')\
                .annotate(numero=Count('product'))\
                .filter(date_expedition__range=(before, now))
            if Company.objects.all():
                company_setup = True
            else:
                company_setup = False
            if Product.objects.all():
                product_setup = True
            else:
                product_setup = False
            ctx = {
                'list_module': list_module,
                'list_tracing': list_tracing,
                'list_expiries': list_expiries,
                'count_for_companies_latest': count_for_companies_latest,
                'count_for_products_latest': count_for_products_latest,
                'company_setup': company_setup,
                'product_setup': product_setup
            }
            return render(request, 'core/index.html', ctx)
        else:
            return render(request, 'core/expired.html')
    else:
        form = MyAuthenticationForm(request, request.POST or None)
        messages = ''
        if form.is_valid():
            if form.get_user() is not None:
                login(request, form.get_user())
                return redirect(request.path)
        ctx = {
            'form': form,
            'message': messages
        }
        return render(request, 'core/login.html', ctx)


def logout_view(request):
    logout(request)
    return redirect('/')