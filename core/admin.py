from django.contrib import admin

from .models import IeswebClient, PaymentHistory, Module


class ModuleAdmin(admin.ModelAdmin):
    list_display = ('name', 'url',)


class IeswebClientAdmin(admin.ModelAdmin):
    list_display = ('client', 'paid_until',)


class PaymentHistoryAdmin(admin.ModelAdmin):
    list_display = ('client', 'payment_date', 'payment_method', 'payment_amount')


admin.site.register(Module, ModuleAdmin)
admin.site.register(IeswebClient, IeswebClientAdmin)
admin.site.register(PaymentHistory, PaymentHistoryAdmin)
