# -*- coding: utf-8 -*-
__author__ = 'gollum23'
__date__ = '5/30/14'
from django.conf.urls import patterns, url

urlpatterns = patterns(
    'core.views',
    url(r'^$', 'home_view', name='home'),
    url(r'^logout/$', 'logout_view', name='logout'),
)